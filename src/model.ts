// this file defines common client/server model interfaces

export interface GameUser {
    username: string
    emo: string
    card?: string
}

export interface GameState {
    name: string
    users: { [username: string] : GameUser }
}
