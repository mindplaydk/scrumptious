import * as express from "express";
import * as http from "http";
import * as socket_io from "socket.io";

import { GameState, GameUser } from "./model";

var app = express();
var server = http.createServer(app);
var io = socket_io(server);

app.use(express.static("public"));

interface GameSocket extends SocketIO.Socket {
    username: string
    game: GameState
}

var _games: { [name: string] : GameState } = {};

function get_game(name: string) : GameState {
    if (!_games[name]) {
        _games[name] = {
            name,
            users: {}
        }
    }

    return _games[name];
}

io.on('connection', (socket: GameSocket) => {
    console.log("client connected");

    function broadcast(name: string, value?: any) {
        console.log("broadcast:", name, value);
        io.sockets.in(socket.game.name).emit(name, value);
    }

    function update_game() {
        broadcast("update game", socket.game);
    }

    socket.on("join game", (name: string) => {
        console.log("join game: ", name);

        socket.game = get_game(name);

        socket.join(name, () => {
            socket.emit("joined game");

            update_game(); // initial first game update
        })
    });

    socket.on("add user", (username: string) => {
        console.log("add user: ", username);

        socket.emit("added user");

        socket.username = username;

        var user: GameUser = {
            username: username,
            emo: "happy"
        };

        socket.game.users[username] = user;

        update_game();
    });

    socket.on("select card", (card: string) => {
        console.log("selected card: ", card);

        socket.game.users[socket.username].card = card;

        update_game();
    });

    socket.on("select emo", (emo: string) => {
        console.log("selected emo: ", emo);

        socket.game.users[socket.username].emo = emo;

        update_game();
    });

    socket.on("restart", () => {
        for (var name in socket.game.users) {
            socket.game.users[name].card = null;
        }

        update_game();

        broadcast("restarted");
    });

    socket.on("disconnect", () => {
        console.log("client disconnected");

        if (socket.username) {
            console.log("remove user: ", socket.username);

            var user = socket.game.users[socket.username];

            delete socket.game.users[socket.username];

            update_game();
        }
    });
});

server.listen(3030, () => {

    var
        host = server.address().address,
        port = server.address().port;

    console.log(`Server listening on http://${host}:${port}`);

});
