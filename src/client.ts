import * as io from "socket.io-client";
import * as $ from "jquery";
import { GameState } from "./model";

import "./base.scss";

var socket = io()

function init() {
    init_socket();
    init_view();
    init_boarding();
    init_login();
    init_restart();
}

function init_boarding() {
    $('#login').hide();
    $('#restart').hide();
    $('#emos').hide();

    $('#boarding').submit((event) => {
        event.preventDefault();
        board($('#gamename').val() as string);
    });

    var gamename = location.hash.substr(1);

    if (gamename) {
        board(gamename);
    }
}

function board(gamename: string) {
    socket.emit("join game", gamename);

    location.hash = gamename;

    $('#boarding').hide();
}

function init_login() {
    $('#login').submit((event) => {
        event.preventDefault();
        login();
    });
}

function login() {
    var username = $('#username').val();

    console.log('logging in: ', username);

    socket.emit("add user", username);

    $('#login').hide();
}

function init_restart() {
    $('#restart').click(() => socket.emit("restart"));
}

var SELECTED = 'is-selected';
var ENABLED = 'is-enabled';
var READY = 'is-ready';
var CARDS = "XS|S|M|L|XL|XXL".split("|");
var EMOS = "happy|wink|sleep|surprised|angry".split("|");

function $emo(emo: string) : JQuery {
    return $('<span class="emo"><span class="icon-emo-' + emo + '"></span></span>');
}

function init_view() {
    var $deck = $('#deck').empty().hide();

    for (var i=0; i<CARDS.length; i++) {
        $deck.append($('<div class="card"/>').text(CARDS[i]).attr('card', CARDS[i]));
    }

    $deck.on('click', '.card', function () {
        if ($('#deck').is('.'+ENABLED)) {
            select_card($(this).attr('card'));
        }
    })

    var $emos = $('#emos');

    for (var i=0; i<EMOS.length; i++) {
        $emos.append($emo(EMOS[i]).attr('emo', EMOS[i]));
    }

    $emos.find('[emo=happy]').addClass(SELECTED); // sloppy.

    $emos.on('click', '.emo', function () {
        select_emo($(this).attr('emo'));
    })
}

function init_socket() {
    socket.on("joined game", () => {
        console.log("joined game");

        $('#login').show();
    })

    socket.on("added user", () => {
        console.log("added user");

        $('#deck').show();
        $('#emos').show();
    })

    socket.on("update game", (game: GameState) => {
        console.log("update game: ", game);

        update_view(game);
    })

    socket.on("restarted", () => {
        console.log("restarted");

        select_card(null);
    })
}

function update_view(game: GameState) {
    var $list = $('#user-list').empty();

    var all_done = true;

    for (var username in game.users) {
        if (! game.users[username].card) {
            all_done = false;
        }
    }

    var any_players = false;

    for (var username in game.users) {
        any_players = true;

        var user = game.users[username];

        var $user = $('<div class="user"/>').text(username);

        $user.prepend($emo(user.emo));

        if (all_done) {
            $user.append($('<span class="card-played"/>').text(user.card));
        } else if (user.card) {
            $user.addClass(READY);
        }

        $list.append($user);
    }

    $('#deck').toggleClass(ENABLED, !all_done);

    $('#restart').toggle(all_done && any_players);
}

function select_card(card?: string) {
    $('#deck .card')
        .removeClass(SELECTED)
        .filter('[card=' + card + ']')
        .addClass(SELECTED);

    if (card) {
        socket.emit("select card", card);
    }
}

function select_emo(emo?: string) {
    $('#emos .emo')
        .removeClass(SELECTED)
        .filter('[emo=' + emo + ']')
        .addClass(SELECTED);

    if (emo) {
        socket.emit("select emo", emo);
    }
}

jQuery(() => init());
