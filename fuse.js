const { FuseBox, CSSPlugin, SassPlugin } = require("fuse-box");

const DEVELOPMENT = process.argv.slice(2).some(v => v === "--dev");

let server_build = FuseBox.init({
    homeDir: "src",
    output: "$name.js",
    target: "server"
});

let server_bundle = server_build
    .bundle("server")
    .instructions(`>[server.ts]`);

let client_build = FuseBox.init({
    homeDir: "src",
    output: "public/$name.js",
    target: "browser",
    plugins : [
         [SassPlugin({
            //outputStyle: 'compressed'
         }), CSSPlugin({
            inject: false,
            outFile: function() { return 'public/poker.css' }
         })]
    ],
    shim: {
        "jquery": {
            exports: "$"
        },
        "socket.io-client": {
            exports: "io"
        }
    }
});

if (DEVELOPMENT) {
    client_build.dev({ port:4444, httpServer: false });
}

let client_bundle = client_build
    .bundle("poker")
    .instructions(`>client.ts`);

if (DEVELOPMENT) {
    server_bundle.watch();
    server_bundle.completed(proc => proc.start());
    // server_bundle.sourceMaps(true); // TODO enabling source-maps causes an error??

    client_bundle.watch().sourceMaps(true); // NOTE: no hrm() in this project because `client.ts` isn't ready for it
}

server_build.run();
client_build.run();
