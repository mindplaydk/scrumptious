Scrum poker in TypeScript under Node.js with Express and Socket.IO
==================================================================

My first Node app and first attempt at TypeScript on the server.

It's a total mess, more or less. You've been warned :-)

To build from sources:

    npm run build

To run in production:

    npm start

To watch and build continuously for development:

    npm run server
